Rails.application.routes.draw do
  devise_for :users, path: 'devise', controllers: {
    sessions: 'user/sessions',
    registrations: 'user/registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  authenticated :user do
    root to: "pages#dashboard"

    resources :users
    resources :books
    resources :legislations
  end

  resources :appointments
  
  root to: "pages#index"

  get "/articles" => "books#articles", as: :articles_path
end

# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
Rails.application.config.assets.precompile += %w(
                                                  admin.js
                                                )

Rails.application.config.assets.precompile += %w(
                                                  admin.css
                                                  theme.scss
                                                  landing.scss
                                                )

Rails.application.config.assets.precompile += %w(
                                                  fallback/default.jpg
                                                  fallback/profile_default.jpg
                                                  fallback/side_bar_default.jpg
                                                  icons/chat.png
                                                  icons/home.png
                                                  icons/gavel.png
                                                  icons/paragraph.png
                                                  icons/chat-selected.png
                                                  icons/home-selected.png
                                                  icons/gavel-selected.png
                                                  icons/paragraph-selected.png

                                                  landing.jpg
                                                  lawyer.jpg
                                                  client.jpg
                                                )

Rails.application.config.assets.precompile += %w(
                                                  urw_gothic_l.ttf
                                                )

require 'rails_helper'

RSpec.describe Book, type: :model do
  context "Uploading a book" do
    it { should validate_presence_of(:author) }
    it { should validate_presence_of(:legislation) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:book) }
  end

  context "Uploaded book" do
    it "should have all the database fields" do
      book = Book.new(:name => "Book", :author => "Author", :legislation => Legislation.new(:name => "1"), book: "file")

      expect(book.valid?).to be_truthy
    end
  end
end

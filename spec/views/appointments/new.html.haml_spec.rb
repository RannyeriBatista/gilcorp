require 'rails_helper'

RSpec.describe "appointments/new", type: :view do
  before(:each) do
    assign(:appointment, Appointment.new(
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :preference => "MyString"
    ))
  end

  it "renders new appointment form" do
    render

    assert_select "form[action=?][method=?]", appointments_path, "post" do

      assert_select "input[name=?]", "appointment[name]"

      assert_select "input[name=?]", "appointment[email]"

      assert_select "input[name=?]", "appointment[phone]"

      assert_select "input[name=?]", "appointment[preference]"
    end
  end
end

require 'rails_helper'

RSpec.describe "appointments/index", type: :view do
  before(:each) do
    assign(:appointments, [
      Appointment.create!(
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :preference => "Preference"
      ),
      Appointment.create!(
        :name => "Name",
        :email => "Email",
        :phone => "Phone",
        :preference => "Preference"
      )
    ])
  end

  it "renders a list of appointments" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone".to_s, :count => 2
    assert_select "tr>td", :text => "Preference".to_s, :count => 2
  end
end

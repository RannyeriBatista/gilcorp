require 'rails_helper'

RSpec.describe "appointments/edit", type: :view do
  before(:each) do
    @appointment = assign(:appointment, Appointment.create!(
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :preference => "MyString"
    ))
  end

  it "renders the edit appointment form" do
    render

    assert_select "form[action=?][method=?]", appointment_path(@appointment), "post" do

      assert_select "input[name=?]", "appointment[name]"

      assert_select "input[name=?]", "appointment[email]"

      assert_select "input[name=?]", "appointment[phone]"

      assert_select "input[name=?]", "appointment[preference]"
    end
  end
end

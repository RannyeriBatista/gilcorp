class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates_presence_of :name, :city, :state, :segment, :role, :required => true, :on => :update
  mount_uploader :avatar, UserAvatarUploader
  self.per_page = 10

  before_create :add_user_role
  before_save :validate_user_fields

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc'  },
    available_filters: [
      :sorted_by,
      :search_query
    ]
  )

  scope :search_query, lambda { |query|
    where("name LIKE ? OR email LIKE ? OR segment LIKE? OR city LIKE? OR state LIKE?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%")
  }

  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^name/
      order("LOWER(users.name) #{ direction  }")
    when /^segment/
      order("LOWER(users.segment) #{ direction  }")
    when /^created_at/
      order("LOWER(users.created_at) #{ direction  }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect  }")
    end
  }

  def self.options_for_sorted_by
    [
      ['Nome (a-z)', 'name_asc'],
      ['Área de atuação (a-z)', 'segment_asc'],
      ['Data de Registro (novos primeiro)', 'created_at_desc'],
      ['Data de Registro (antigos primeiro)', 'created_at_asc']
    ]
  end

  def add_user_role
    self.role = "user" if not self.role.present?
  end

  def validate_user_fields
    self.status = "active" if self.name.present?
  end
end

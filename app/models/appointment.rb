class Appointment < ApplicationRecord
  validates_presence_of :name, :email, :phone, :demand, :preference
  self.per_page = 10

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc'  },
    available_filters: [
      :sorted_by,
      :search_query
    ]
  )

  scope :search_query, lambda { |query|
    where("name LIKE ? OR email LIKE ? OR demand LIKE? OR phone LIKE? OR preference LIKE?", "%#{query}%","%#{query}%","%#{query}%","%#{query}%","%#{query}%")
  }

  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^name/
      order("LOWER(appointments.name) #{ direction }")
    when /^preference/
      order("LOWER(appointments.preference) #{ direction }")
    when /^created_at/
      order("LOWER(appointments.created_at) #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect  }")
    end
  }

  def self.options_for_sorted_by
    [
      ['Nome (a-z)', 'name_asc'],
      ['Forma de contato (a-z)', 'preference_asc'],
      ['Data de Registro (novos primeiro)', 'created_at_desc'],
      ['Data de Registro (antigos primeiro)', 'created_at_asc']
    ]
  end
end

class Book < ApplicationRecord
  belongs_to :legislation

  mount_uploader :book, FilesUploader

  validates_presence_of :author, :legislation, :name, :book
end

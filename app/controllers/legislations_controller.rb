class LegislationsController < ApplicationController
  before_action :authenticate_user!
  before_action :restrict_actions

  # POST /legislations
  # POST /legislations.json
  def create
    @legislation = Legislation.new(legislation_params)

    respond_to do |format|
      if @legislation.save
        format.html { redirect_to books_path, flash: {success: 'Legislation was successfully created.'} }
        format.json { render :show, status: :created, location: @legislation }
      else
        format.html { redirect_to books_path, flash: {danger: 'Error, please verify your form and try again.'} }
        format.json { render json: @legislation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /legislations/1
  # PATCH/PUT /legislations/1.json
  def update
    respond_to do |format|
      if (@legislation.update_without_password(legislation_params) and current_legislation.role == 'Boss') or (@legislation.update(legislation_params))
        format.html { redirect_to books_path, flash: {success: 'Legislation was successfully updated.'} }
        format.json { render :show, status: :ok, location: @legislation }
      else
        format.html { redirect_to books_path, flash: {danger: 'Error, please verify your form and try again.'} }
        format.json { render json: @legislation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /legislations/1
  # DELETE /legislations/1.json
  def destroy
    @legislation.destroy
    respond_to do |format|
      format.html { redirect_to books_path, flash: {success: 'Legislation was successfully destroyed.'} }
      format.json { head :no_content }
    end
  end

  # This action avoid common legislations to have access to important actions in this controller
  def restrict_actions
    redirect_to "/" if current_user.role == "user"
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def legislation_params
      params.require(:legislation).permit(:name)
    end
end

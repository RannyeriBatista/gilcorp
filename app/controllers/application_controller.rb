class ApplicationController < ActionController::Base
  # This action is to avoid users to show up without all the required fields
  def validate_profile_fields
    redirect_to edit_user_path(current_user) if not current_user.name.present?
  end

  # This action avoid common users to have access to important actions in this controller
  def restrict_actions
    if current_user
      redirect_to "/" if not current_user.role == "Boss"
    else
      redirect_to "/"
    end
  end
end

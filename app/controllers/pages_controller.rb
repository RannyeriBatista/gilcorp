class PagesController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :validate_profile_fields, except: [:index]

  def index
    render layout: "landing"
  end

  def dashboard
    @user = current_user
  end

  def validate_profile_fields
    redirect_to edit_user_path(current_user) if not current_user.name.present?
  end
end

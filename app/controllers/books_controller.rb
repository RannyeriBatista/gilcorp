class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :restrict_actions, except: [:show]

  # GET /books
  # GET /books.json
  def index
    @books = Book.all
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books
  # POST /books.json
  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to books_path, flash: {:success => 'Book was successfully created.'} }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { redirect_to books_path, flash: {:danger => 'Error, please verify your form and try again.'} }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to books_path, flash: {:success => 'Book was successfully updated.'} }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { redirect_to books_path, flash: {:danger => 'Error, please verify your form and try again.'} }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, flash: {:success => 'Book was successfully destroyed.'} }
      format.json { head :no_content }
    end
  end

  def articles
  end

  # This action avoid common users to have access to important actions in this controller
  def restrict_actions
    redirect_to "/articles" if current_user.role == "user"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:author, :legislation_id, :name, :book)
    end
end

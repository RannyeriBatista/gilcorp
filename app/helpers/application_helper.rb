module ApplicationHelper
  def link_active(path)
    if current_user
      case path
      when "home"
        return "icon--home--selected" if current_page?("/") || current_page?(edit_user_path(current_user))
      when "users"
        return "icon--active" if current_page?(users_path)
      when "appointments"
        return "icon--active" if current_page?(appointments_path)
      when "paragraph"
        return "icon--paragraph--selected" if current_page?(controller: 'books')
      # when "gavel"
      #   return "icon--home--selected" if current_page?(controller: 'user')
      # when "chat"
      #   return "icon--home--selected" if current_page?(controller: 'user')
      end
    end
  end
end

json.extract! appointment, :id, :name, :email, :phone, :preference, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)

json.extract! book, :id, :author, :legislation_id, :name, :book, :created_at, :updated_at
json.url book_url(book, format: :json)

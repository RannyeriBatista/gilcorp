class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :author
      t.belongs_to :legislation, foreign_key: true
      t.string :name
      t.string :book

      t.timestamps
    end
  end
end
